// /**
//  * Share and enjoy, don't copy.
//  * (C) New York Ave., LLC 2016
//  * Deland, Florida, USA
// */
//
// // ******************INSTRUCTIONS****************** \\
// // There are two nav styles in this template:
// // "nav" and "nav-fullscreen"
// //
// // This code is for "nav-fullscreen".
// //
// // When switching to this nav style, remember to:
// // * change the partial links in the HTML
// // * comment in/out the appropriate SCSS and Javascript
// // * change which nav is loaded in main.js
// // ************************************************* \\
//
// function openNav()
// {
//   // Prevent jump
//   if (jQuery(window).height() < jQuery('html').height())
//   {
//     jQuery('html').addClass('scrollbar');
//   }
//   // Add the classes to open nav and change hamburger to an 'x'
//   jQuery('body').toggleClass('nav-open')
//     .find('.hamburger').toggleClass('is-active');
// }
//
// function initNavFullscreen()
// {
//   jQuery('.hamburger').click(openNav);
// }
