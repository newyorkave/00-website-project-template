/**
 * Share and enjoy, don't copy.
 * (C) New York Ave., LLC 2016
 * Deland, Florida, USA
*/

jQuery(document).ready(function()
{
  // Comment in/out the modules you'd like to use.
  // Don't use both navigation types at the same time.
  initBlogPost();
  initBlogonizer();
  initBlogRssBoxes();
  initNav();
  // initNavFullscreen();
  initNavPlugin();
  initScrollTo();
});
