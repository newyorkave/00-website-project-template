/**
 * This widget is dependent upon the functions in the blog.js file.
 * Share and enjoy, don't copy.
 * (C) New York Ave., LLC 2016
 * Deland, Florida, USA
*/

function initBlogRssBoxes()
{
  //Get the RSS response and then process it.
  getRSS("https://www.ny-ave.com/1/feed").then(function(response) {
    //This is the contents of a weebly RSS element
    var rssUL = jQuery(".rss-items li.rss-item");
    //An object representing the individual posts in an RSS feed
    var posts = response.getElementsByTagName("item");
    jQuery(posts).each(function(idx, post){
      var content = sanitizeHttpCurrentDomain(getContent(post)); //Get post contents
      var html = sanitizeHttpCurrentDomain(postHTML(post)); //Create RSS "card" HTML
      var postImage = sanitizeHttpCurrentDomain(getImgURL(content)); //Get post <img> SRC
      //Replaces feed reader contents with RSS "cards" generated from postHTML()
      setBGImg(idx, rssUL, postImage, html);
    });
  });
}

/**
 * Replaces the contents of the RSS feed with the HTML cards we created
 * in postHTML(). Appllies a BG images to these cards afterwards.
 * @param {number} index   Index of the element to be replaced.
 * @param {object} rssList Collection of <li>s in the RSS feed.
 * @param {string} imgSrc  The SRC of the image to be used as a BG
 * @param {string} content RSS "card" contents created from postHTML();
 */
function setBGImg(index, rssList, imgSrc, content){
  var postContainer = jQuery(rssList)[index];
  if(postContainer){
    jQuery(postContainer).find("a").attr("target", "_self");
    jQuery(postContainer).find("a").removeAttr("title");
    jQuery(postContainer).find("a").html(content);
    jQuery(postContainer)[0].style.setProperty( 'background-image', 'url('+imgSrc+')', 'important' );
    jQuery(postContainer)[0].style.setProperty( 'background-size', 'cover', 'important' );
    jQuery(postContainer).css({
      "background-repeat": "no-repeat",
      "background-position": "50% 50%"
    });
  }
}
