Project Template by New York Ave
===

This repository is a template for starting a new website at [New York Ave](https://www.ny-ave.com/). It supports a [wide](https://www.holderpest.com/) [variety](https://www.marketing2go.biz/) of [applications](https://www.summerlinbenefitsconsulting.com/) in [the wild](http://www.floridaaquaticgems.com/) and is designed to work on [Weebly](https://www.weebly.com), empowering marketers, designers, and business owners with a CMS that just works.

FAQ
===

If you have any questions that aren't covered below, feel free to contact Christopher Williams.

## What must be installed before I set up this template?

* Git
* Node.js
* npm
* gulp

## How do I set up this template?

* In your console, cd to the directory where you want to keep this template.
* Clone this repo to your computer.
* For the purpose of this tutorial, things in curly braces should be replaced with the appropriate replacement. For example, if you want to call your project my-first-nya-project, the thing in curly braces below that says {project-name} should be replaced with my-first-nya-project.
```
git clone https://bitbucket.org/newyorkave/00-website-project-template.git {project-name}
```
* To install this template's dependencies, run:
```
npm install
```

## How do I set up this template as a new project for NYA?

* In your console, and once you have completed the above instructions, cd into your project.
* Delete the .git folder to start a fresh Git project.
```
cd {project-name}
rm -rf .git
```
* Initialize your new Git project.
* Add all the files.
* Make your initial commit.
```
git init
git add --all
git commit -m "Initial Commit"
```
* In the Web Design folder of the New York Ave BitBucket account, create a new project. It should be the same as the folder you created locally that we've been calling project-name.
* Now add that as the origin and push it up. You can copy this link from the top right corner of the new project you created on bitbucket.org
```
git remote add origin https://{yourusername}@bitbucket.org/newyorkave/{project-name}.git
git push -u origin --all
git push origin --tags
```
Now you are ready to work locally. Remember to commit after each issue is resolved with a descriptive commit message and push your changes up to the repo at the end of each day.

After all this is done, don't forget to update your new repo's README appropriately.

## How do I import this theme into Weebly?

* In the Weebly editor, go to the 'Theme' tab.
* In the sidebar click 'Change Theme'.
* Click 'Import Theme' and select the ZIP file located in the theme's *dist* directory.

The custom theme is now available in the 'custom' tab of the 'Change Theme' menu.

After you have imported the theme, be sure to copy the contents of any files from *weebly_partials* into the files in the Weebly editor.

You can make incremental updates by copying the contents of CSS, HTML, JS files from the *dist* directory into the files in the Weebly editor.

## Notes:

As of 9/5/17, node-sass wasn't ready for npm >= 5. If you're getting errors on install that say `gyp verb which failed`, downgrade to npm 4 or 3 by running `npm install -g npm@4.6.1`. Then remove your node_modules folder and run `npm install` again.

## Who do I talk to if I have a question?

* If you have any questions contact Christopher Williams.
