/**
 * Share and enjoy, don't copy.
 * (C) New York Ave., LLC 2016
 * Deland, Florida, USA
*/

function initBlogPost()
{  //Code to be executed on a blog post page
  if(jQuery("body.wsite-blog-post").length){
    setBannerImage(jQuery(".blog-content > div"));
    setReadTime(jQuery(".blog-content > div"));
  }
}

/**
 * A function that finds an <img> tag within a jQuery object and append it to the
 * header area of a blog post.
 * @param {object} elements A jQuery object representing the HTML content of a post.
 */
function setBannerImage(elements){
  elements.each(function(){
    var img = jQuery(this).find("img");
    if(img.length){
      jQuery(img[0]).removeAttr("style");
      var bgImg = '<div class="bg-img-wrap">' + img[0].outerHTML + '</div>';
      jQuery(".blog-header").append(bgImg);

      return false;
    }
  });
}

/**
*   Function replaces the generated title with the title of the blog post.
*  @param {object} elements A jQuery object representing the HTML content of a post
**/
function setBannerTitle(elements) {
  elements.each(function() {
    var title = jQuery(this).find(".blog-title-link");
    console.log(title.text());
    if(title.length) {
      var text = jQuery(title).text();
      jQuery('#header .wsite-content-title').replaceWith('<h2 class="wsite-content-title">'+text+'</h2>');
      return false;
    }
  });
}

/**
 * A function that sets the read time of an object representing HTML content
 * @param {object} elements A jQuery object representing the HTML content of a post.
 */
function setReadTime(elements) {
  var wordCount = 0;
  var time = 0;

  //Iterate over each element in the object
  elements.each(function() {
    var wom, words;
    if (jQuery(this).hasClass("paragraph")) {
      //This element is a paragraph. Get a wordcount and add it
      //to the total.
      wom = jQuery(this)[0].innerHTML.match(/\S+/g);
      words = wom.length;
      wordCount += words;
    } else {
      // This element is a title. Get a wordcount and add it to the total.
      var title = jQuery(this).find(".wsite-content-title");

      if(title.length){
        wom = title[0].innerHTML.match(/\S+/g);
        words = wom.length;
        wordCount += words;
      }
    }
  });

  //Averge person reads 200 words per minute
  time = Math.round(wordCount/200);

  //Write this text to a specific div in the DOM.
  jQuery(".read-time .time-in-mins").text(time + " minute read");
  jQuery(".read-time").addClass("time-set");
}
