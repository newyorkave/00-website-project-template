/**
 * Share and enjoy, don't copy.
 * (C) New York Ave., LLC 2016
 * Deland, Florida, USA
*/

// ******************INSTRUCTIONS****************** \\
// There are two nav styles in this template:
// "nav" and "nav-fullscreen"
//
// This code is for "nav".
//
// When switching to this nav style, remember to:
// * change the partial links in the HTML
// * comment in/out the appropriate SCSS and Javascript
// * change which nav is loaded in main.js
// ************************************************* \\

function initNav()
{
  jQuery(".mobile-controls").on("click", function(){
    jQuery("#nav").toggleClass("active");
  });

  jQuery(window).on("load", function() {
      var submenus = jQuery("#wsite-menus ul");
      var main_menu_li = jQuery(".wsite-menu-default li");

      jQuery.each(submenus, function(idx, menu) {
          var showmore_html = "<span class='submenu__show-more'></span>";
          var mySubmenu = jQuery(menu).clone();

          jQuery(main_menu_li[idx]).append(showmore_html);
          jQuery(main_menu_li[idx]).append(mySubmenu);
      });

      jQuery(".wsite-menu-default .submenu__show-more").on("click", function(e) {
        jQuery(this).parent("li")
                    .toggleClass("submenu--open")
                    .siblings("li")
                    .removeClass("submenu--open");
      });
  });
}
