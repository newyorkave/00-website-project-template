/**
 * This file contains blog-related dependencies.
 * For example, blog-rss-boxes.js depends on several functions here.
 * Share and enjoy, don't copy.
 * (C) New York Ave., LLC 2016
 * Deland, Florida, USA
*/

/**
 * Returns the HTML contents of an RSS post.
 * @param  {string} content Individual <item> tag from RSS feed.
 * @return {string}         Text representation of contents of post HTML.
 */
function getContent(content) {
  var contentHTML = jQuery(content).find("content\\:encoded, encoded").text();
  return contentHTML;
}

/**
 * Builds HTML for RSS "card" that contains the post title, img,
 * date, etc.
 * @param  {string} content Individual <item> tag from RSS feed.
 * @return {string}         HTML of RSS "card"
 */
function postHTML(content) {
  //Various metadata.
  var title = jQuery(content).find("title").text(),
      link = jQuery(content).find("link").text(),
      category = jQuery(content).find("category").first().text(),
      pubDate = jQuery(content).find("pubDate").text(),
      description = jQuery(content).find("description").text(),
      contentHTML = jQuery(content).find("encoded").text();

      var date = new Date(pubDate);
      var newDate = (date.getMonth() + 1) + "/" + date.getDate() + "/" + date.getFullYear().toString().substr(2,2);

  // Build the HTML string to return
  var html = "<span class='post-content'>" +
                "<span class='post-category'>in <span>" + category + "</span></span>" +
                "<span class='post-title'>" + title + "</span>" +
                "<span class='post-date'>" + newDate + "</span>" +
             "</span>";
  // console.log(content);
  // console.log(html);
  return html;
}

/**
 * Iterate through the contents of a post and return the SRC of the first
 * <img> that is found.
 * @param  {string} htmlString The HTML contents of an individual RSS post.
 * @return {string}            [description]
 */
function getImgURL(htmlString) {
  var img, elements = jQuery(htmlString);
  elements.each(function(idx, val){
    img = jQuery(val).find("img");
    if(img.length){
      return false;
    }
  });
  return img.attr('src');
}

/**
 * Performs an XMLHTTP request on the provided URL
 * @param  {string} url The URL to be queried
 * @return {promise}
 */
function getRSS(url) {
  // Return a new promise.
  return new Promise(function(resolve, reject) {
    // Do the usual XHR stuff
     var req = new XMLHttpRequest();
     req.open('GET', url);

     req.onload = function() {
       // This is called even on 404 etc
       // so check the status
       if (req.status == 200) {
         // Resolve the promise with the response text
         resolve(req.responseXML);
       }
       else {
         // Otherwise reject with the status text
         // which will hopefully be a meaningful error
         reject(Error(req.statusText));
       }
     };

     // Handle network errors
     req.onerror = function() {
       reject(Error("Network Error"));
     };

     // Make the request
     req.send();
  });
}
