/*
Responsive navigation written by Martin Blackburn.
www.martinblackburn.co.uk

Feel free to use this for your own projects, just be nice and link back here, or my site :)
*/
function initNavPlugin() {
  ResponsiveNav = function(nav, breakPoint)
  {
      //if no nav container, output an error
      if(typeof nav === "undefined")
      {
          console.log("The nav container needs setting.");
          return false;
      }

      //elements
      var navControl = nav.find(".navControl").first();
      var mainUL = nav;//nav.find("ul").first();
      var mainLIs = mainUL.children();
      var extraLI = $("<li class='extraDropdown'><a href='#'>More <span><i class='fa fa-angle-down'></i></span></a>");
      var extraDropdown = $("<ul class='dropdown'></ul>");
      extraLI.append(extraDropdown);

  	//variables
      var breakPointVal = (typeof breakPoint != "number") ? 500 : breakPoint;
  	var siteWidth = $(document).width();
  	var lastSiteWidth = null;
  	var navWidth = mainUL.width();
  	var usingExtraDropdown = false;

  	//listener for screen width
  	$(window).resize(function() {
  		siteWidth = $(document).width();
  		navWidth = mainUL.width();
  		checkNavType();
  		lastSiteWidth = siteWidth;
  	});

  	//toggle nav when nav control is clicked
  	navControl.on('click', function(event) {
  	    event.preventDefault();
          toggleNav();
      });

  	//check if to use mobile nav or not
      checkNavType();

      //added a extra dropdown if not already there
      function addExtraDropdown()
      {
          if (!usingExtraDropdown) {
              usingExtraDropdown = true;
              mainUL.append(extraLI);
              extraLI.hover(function() {
                extraLI.toggleClass('active');
                extraLI.find("i.fa")
                       .toggleClass("fa-angle-down")
                       .toggleClass("fa-angle-up");
              });
          }
      }

      //make sure the LIs fit into the nav
      function checkLIsFit()
      {
          var widthLIs = 0;

          mainLIs = mainUL.children();

          mainLIs.each(function() {
              widthLIs += $(this).outerWidth(true);
          });

          //need a dropdown
          if(widthLIs > navWidth) {
              addExtraDropdown();
              moveLI();
          }
      }

      //move LIs to the extra dropdown from main nav
      function moveLI()
      {
          mainLIs = mainUL.children().not(".extraDropdown");

          extraDropdown.prepend(mainLIs.last());

          checkLIsFit();
      }

      //move all LIs from extra dropdown back to the nav
      function resetExtraDropdown()
      {
          usingExtraDropdown = false;

          var LIsToMove = extraDropdown.children();

          mainUL.find(".extraDropdown").remove();

          mainUL.append(LIsToMove);
      }

      //check if to use mobile nav or not
      function checkNavType()
      {
          if(siteWidth != lastSiteWidth)
          {
          	if(siteWidth >= breakPointVal)
          	{
          	    navControl.hide();
          	    mainUL.show();
          	}
          	else {
          	    navControl.show();
          	    mainUL.hide();
          	}

          	resetExtraDropdown();
          	checkLIsFit();
          }
      }

  	//open or close nav
      function toggleNav()
      {
          mainUL.slideToggle();
      }
  };

  jQuery('ul.wsite-menu-default').each(function() {
    //pass the nav element and set the break point.
    new ResponsiveNav(jQuery(this), 500);
  });
}
