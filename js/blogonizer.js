/**
 * Share and enjoy, don't copy.
 * (C) New York Ave., LLC 2016
 * Deland, Florida, USA
*/

function initBlogonizer()
{
  // Set the default section to open on load.
  var sectionToOpenOnLoad = "search";
  var sectionClicked, height;

  // This code block is where everything happens.
  // The four functions below are the tools to make it work.
  if(jQuery(".blogonizer").length > 0) {
    // If the button clicked is not already selected,
    // - toggle the button to selected
    // - toggle the content to show, hide others
    jQuery("button.categories").click(blogMetaToggle);
    jQuery("button.archives").click(blogMetaToggle);
    jQuery("button.search").click(blogMetaToggle);

    // If window is greater than tablet width and one of the three
    // sections is defined, open the default section on page load.
    if(( jQuery(window).width() > 992 ) &&
       ( sectionToOpenOnLoad == "categories" ||
         sectionToOpenOnLoad == "archives" ||
         sectionToOpenOnLoad == "search" )
      ){
      jQuery("button." + sectionToOpenOnLoad).addClass("selected");
      jQuery(".blogonizer__" + sectionToOpenOnLoad).addClass("visible");
      height = jQuery(".blogonizer__" + sectionToOpenOnLoad).height();
      resizeContent();
    }
  }

  // Select the button and deselect its siblings.
  function buttonToggle(el){
    if(el.hasClass("selected")) {
      el.removeClass("selected");
    }
    else {
      el.siblings().removeClass("selected");
      el.toggleClass("selected");
    }
  }

  // Resize .blogonizer__content to the height provided,
  // which will be the height of the active section.
  function resizeContent(){
    jQuery(".blogonizer__content").css({
      "max-height": height + "px",
      "height": height + "px",
      "padding-top": "60px",
      "padding-bottom": "60px"
    });
  }

  // Show selected content and hide other content.
  function contentToggle(el){
    if(el.hasClass("visible")) {
      console.log('shrinking...');
      el.removeClass("visible");
      $(".blogonizer__content").css({
        "height":"0px",
        "max-height":"0px",
        "padding-top": "0px",
        "padding-bottom": "0px"
      });
    }
    else {
      height = el.height();
      if(el.siblings().hasClass("visible")){
        el.siblings().removeClass("visible");
      }
      el.toggleClass("visible");
      if(el.hasClass("visible")){
        resizeContent();
      }
    }
  }

  // Toggle the button to selected and the content to visible; hide others.
  function blogMetaToggle() {
      buttonToggle(jQuery(this));
      sectionClicked = this.dataset.blogonizer;
      contentToggle(jQuery(".blogonizer__" + sectionClicked));
  }

}
